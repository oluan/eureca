# **YES**OR**NO** APP #
## Luan Andrade - 2016 ##
Desenvolvido para o Desafio Eureca

## Tecnologias utilizadas: ##
### Web: ###
* Framework [CakePHP 2.x](http://book.cakephp.org/2.0/pt/index.html)
* [Banco de dados Mysql](https://www.mysql.com/)
* HTML5
* CSS
* [Jquery](https://jquery.com/)

### App:###
* [Ionic/AngularJS](http://ionicframework.com)
* [API Backand](http://backand.com)

## Funcionalidades: ##
### Web: ###
* Acesso administrativo protegido por senha (senhas criptografadas)
* Visão geral das perguntas
* Criação/Alteração de perguntas
* Gerenciamento de usuários: Visao geral, edição, deleção, criação, controle de permissões
* Relatório 1: Gráficos de Porcentagem de respostas x pergunta
* Relatório 2: Gráficos de porcentagem por tipo de resposta
![Login](/screens/web_login.png?raw=true "Login")
![Visão Geral](/screens/web_perguntas_visao_geral.png?raw=true "Visão geral")
![Relatorio 1](/screens/web_perguntas_relatorio1.png?raw=true "Relatorio 1")
![Relatório 2](/screens/web_perguntas_relatorio2_1.png?raw=true "Relatório 2")

### App: ###
* Login de usuário
* Cadastro
* Interagir com as perguntas movendo os cards para esquerda e direita para responder respectivamente sim e não

![Inicio](/screens/app_inicio.png?raw=true "Inicio")
![Login](/screens/app_login.png?raw=true "Login")
![Card](/screens/app_card.png?raw=true "card")
![Swipe](/screens/app_card_right.png?raw=true "Swipe")