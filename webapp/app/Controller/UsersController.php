<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
public $components = array('Paginator');


public function beforeFilter() {
	parent::beforeFilter();
	$this->Auth->allow('add','logout','login');
}

/**
 * index method
 *
 * @return void
 */
public function index() {
	$this->User->recursive = 0;
	$this->set('users', $this->Paginator->paginate());
}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function view($id = null) {
	if (!$this->User->exists($id)) {
		throw new NotFoundException(__('Invalid user'));
	}
	$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
	$this->set('user', $this->User->find('first', $options));
}

/**
 * add method
 *
 * @return void
 */
public function add() {
	if ($this->request->is('post')) {
		$this->User->create();
		if ($this->User->save($this->request->data)) {
			$this->Flash->success(__('Dados salvos.'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Flash->error(__('Usuário não pode ser salvo. Tente novamente.'));
		}
	}
	$types = $this->User->Type->find('list');
	$this->set(compact('types'));
}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function edit($id = null) {
	if (!$this->User->exists($id)) {
		throw new NotFoundException(__('Invalid user'));
	}
	if ($this->request->is(array('post', 'put'))) {
		if ($this->User->save($this->request->data)) {
			$this->Flash->success(__('Dados salvos.'));
			return $this->redirect(array('action' => 'index'));
		} else {
			$this->Flash->error(__('Usuário não pode ser salvo. Tente novamente.'));
		}
	} else {
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->request->data = $this->User->find('first', $options);
	}
	$types = $this->User->Type->find('list');
	$this->set(compact('types'));
}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
public function delete($id = null) {
	$this->User->id = $id;
	if (!$this->User->exists()) {
		throw new NotFoundException(__('Invalid user'));
	}
	$this->request->allowMethod('post', 'delete');
	if ($this->User->delete()) {
		$this->Flash->success(__('Usuário deletado.'));
	} else {
		$this->Flash->error(__('Usuário não pode ser deletado. Tente novamente.'));
	}
	return $this->redirect(array('action' => 'index'));
}



	/**
	 * login method
	 *
	 * @throws NotFoundException
	 * @param string $id
	 * @return void
	 */

	public function login() {
		//$this->set('title_for_layout', 'Bem vindo ao MURCS - Acesse já');
		$this->layout='login';
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				$this->redirect(array('controller'=>'questions','action' => 'index'));
			} else {
				$this->Flash->error(__('Usuário ou senha errados. Tente novamente.'));
			}
		}
	}


	public function logout() {
		$this->redirect($this->Auth->logout());
	}


}
