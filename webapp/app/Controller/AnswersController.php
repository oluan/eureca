<?php
App::uses('AppController', 'Controller');
/**
 * Answers Controller
 *
 * @property Answer $Answer
 * @property PaginatorComponent $Paginator
 */
class AnswersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Answer->recursive = 0;
		$this->set('answers', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Answer->exists($id)) {
			throw new NotFoundException(__('Invalid answer'));
		}
		$options = array('conditions' => array('Answer.' . $this->Answer->primaryKey => $id));
		$this->set('answer', $this->Answer->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Answer->create();
			if ($this->Answer->save($this->request->data)) {
				$this->Flash->success(__('The answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The answer could not be saved. Please, try again.'));
			}
		}
		$users = $this->Answer->User->find('list');
		$questions = $this->Answer->Question->find('list');
		$this->set(compact('users', 'questions'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Answer->exists($id)) {
			throw new NotFoundException(__('Invalid answer'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Answer->save($this->request->data)) {
				$this->Flash->success(__('The answer has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The answer could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Answer.' . $this->Answer->primaryKey => $id));
			$this->request->data = $this->Answer->find('first', $options);
		}
		$users = $this->Answer->User->find('list');
		$questions = $this->Answer->Question->find('list');
		$this->set(compact('users', 'questions'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Answer->id = $id;
		if (!$this->Answer->exists()) {
			throw new NotFoundException(__('Invalid answer'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Answer->delete()) {
			$this->Flash->success(__('The answer has been deleted.'));
		} else {
			$this->Flash->error(__('The answer could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}



	public function dashboard1(){

		$questions = $this->Answer->Question->find('list');
		$this->set(compact('questions', $questions));

		if($this->request->is('post')){
			$id= $this->request->data['Answer']['questions'];
			$question = $this->Answer->Question->find('first',array('conditions'=>array('Question.id'=>$id)));
			$this->set(compact('question', $question));

		} else {
			$question = $this->Answer->Question->find('first');
			$this->set(compact('question', $question));
		}

	}

	public function dashboard2(){

	}

	public function rest1($id = null){
		$this->layout='';
		$options1 = array('conditions'=>array('Answer.question_id'=>$id,
			'Answer.text'=>'yes'
			),
			'fields'=>array('text')
			);
		$yes = $this->Answer->find('list',$options1);
		$this->set('yes',$yes);

		$options2 = array('conditions'=>array('Answer.question_id'=>$id,
			'Answer.text'=>'no'
			),
			'fields'=>array('text')
			);
		$no = $this->Answer->find('list',$options2);
		$this->set('no',$no);

	}

	public function rest2(){
		$this->layout='';

		//array respostas positivas
		$options1 = array(
			'conditions'=>array('Answer.text'=>'yes'),
			'fields'=>array('question_id','COUNT(*) as count'),
			'group'=>array('question_id')
			);
		$yes = $this->Answer->find('all',$options1);
		$arr_yes = array();

		foreach ($yes as $y) {
			array_push($arr_yes, array('name'=>"Pergunta #".$y['Answer']['question_id'], 'data'=>array(intval($y[0]['count']))));
		}
		$this->set('yes',$arr_yes);

		//array respostas negativas
		$options2 = array(
			'conditions'=>array('Answer.text'=>'no'),
			'fields'=>array('question_id','COUNT(*) as count'),
			'group'=>array('question_id')
			);
		$no = $this->Answer->find('all',$options2);
		$arr_no = array();

		foreach ($no as $n) {
			array_push($arr_no, array('name'=>"Pergunta #".$n['Answer']['question_id'], 'data'=>array(intval($n[0]['count']))));
		}
		$this->set('no',$arr_no);

		$result = array('yes'=>$arr_yes,'no'=>$arr_no);
		$this->set('result',$result);


	}
}
