<div class="header">
	<h2>Perguntas <strong>Gerenciar</strong></h2>
</div>
<div class="row">
	<div class="col-md-12 portlets">
		<div class="panel">
			<div class="panel-header panel-controls">
				<h3 class="pull-left"><i class="fa fa-plus"></i> <strong>Perguntas</strong> Adicionar</h3>
			</div>
			<div class="panel-content">

				<?php echo $this->Form->create('Question', array('url' => 'add', 'class'=>'form-horizontal')); ?>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-question"></i>			
								<?php echo $this->Form->input('title',array('class'=>'form-control form-white','placeholder'=>'Digite sua pergunta','label'=>false)); ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-pencil"></i>			
								<?php echo $this->Form->input('description',array('class'=>'form-control form-white','placeholder'=>'Descrição da pergunta','label'=>false)); ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-calendar"></i>			
								<?php echo $this->Form->input('date_inclusion',array('type'=>'text','class'=>'form-control form-white','placeholder'=>'Data de Inclusão. (formato aaaa-mm-dd)','label'=>false)); ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-calendar"></i>			
								<?php echo $this->Form->input('date_exclusion',array('type'=>'text','class'=>'form-control form-white','placeholder'=>'Data limite. (formato aaaa-mm-dd)','label'=>false)); ?>
							</div>
						</div>

						
					</div>
					
				</div>
				
				<?php echo $this->Form->submit('Enviar',array('class'=>'btn btn-lg btn-success btn-block ladda-button',"data-style"=>"expand-left")) ?>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
</div>
