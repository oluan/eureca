

<div class="header">
	<h2>Perguntas <strong>Gerenciar</strong></h2>
</div>

<div class="row">
	<div class="col-md-12 portlets">
		<div class="panel">
			<div class="panel-header panel-controls">
				<h3 class="pull-left"><i class="fa fa-table"></i> <strong>Perguntas</strong> Visão Geral</h3>
				<?php echo $this->Html->link(("<i class='fa fa-plus'></i> Pergunta"), array('action' => 'add'),array("class"=>"btn btn-primary pull-right",'escape'=>false)); ?>
			</div>
			<div class="panel-content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('title','título'); ?></th>
							<th><?php echo $this->Paginator->sort('description','descrição'); ?></th>
							<th><?php echo $this->Paginator->sort('date_inclusion','data de inclusão'); ?></th>
							<th><?php echo $this->Paginator->sort('date_exclusion','data de exclusão'); ?></th>
							<th><?php echo $this->Paginator->sort('active','ativa'); ?></th>
							<th class="actions"><?php echo __('Ações'); ?></th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($questions as $question): ?>
						<tr>
							<td><?php echo h($question['Question']['id']); ?>&nbsp;</td>
							<td><?php echo h($question['Question']['title']); ?>&nbsp;</td>
							<td><?php echo h($question['Question']['description']); ?>&nbsp;</td>
							<td><?php echo h($question['Question']['date_inclusion']); ?>&nbsp;</td>
							<td><?php echo h($question['Question']['date_exclusion']); ?>&nbsp;</td>
							<td><?php echo h($question['Question']['active']); ?>&nbsp;</td>
							<td class="actions">
								<?php echo $this->Html->link(("<i class='fa fa-pencil-square-o'></i>"), array('action' => 'edit', $question['Question']['id']),array("class"=>"btn btn-sm btn-warning",'escape'=>false)); ?>
								
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<p>
				<?php
				echo $this->Paginator->counter(array(
					'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} , começando em {:start}, terminando em {:end}')
					));
					?>
				</p>
				<div class="paging">
					<ul class='pagination'>

						<?php
						echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('próximo') . ' >', array(), null, array('class' => 'next disabled'));
						?>
					</ul>
				</div>

			</div>
		</div>
	</div>
</div>
