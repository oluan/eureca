      <!-- BEGIN SIDEBAR -->
      <div class="sidebar">
        <div class="logopanel">
          <h1><?php echo $this->Html->link(("<strong>YES</strong>OR<strong>NO</strong>"), array('controller'=>'questions','action' => 'index'),array("style"=>"color:white",'escape'=>false)); ?></h1>
        </div>
        <div class="sidebar-inner">
          <div class="sidebar-top">
            <form action="#" method="post" class="searchform" id="search-results">
              <input type="text" class="form-control" name="keyword" placeholder="Search here...">
            </form>
            <div class="userlogged clearfix">
              <i class="icon icons-faces-users-01"></i>
              <div class="user-details">
                <h4>Admin</h4>
                <div class="dropdown user-login">
                  <button class="btn btn-xs dropdown-toggle btn-rounded" type="button" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" data-delay="300" aria-expanded="false">
                    <i class="online"></i><span>Online</span><i class="fa fa-angle-down"></i>
                  </button>
                  <ul class="dropdown-menu">
                    <li><a href="#"><i class="busy"></i><span>Ocupado</span></a></li>
                    <li><a href="#"><i class="turquoise"></i><span>Invisível</span></a></li>
                    <li><a href="#"><i class="away"></i><span>Ausente</span></a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <div class="menu-title">
            <span>Menu</span> 
          </div>
          <ul class="nav nav-sidebar">
            <li class="tm nav-active active">

              <?php echo $this->Html->link("<i class='fa fa-question'></i> <span>Perguntas</span>", array("controller"=>"questions","action" => "index"),array('escape'=>false)); ?>
            </li>
            <li class="tm nav-parent">
              <?php echo $this->Html->link("<i class='fa fa-users'></i> <span>Usuários</span>", array("controller"=>"users","action" => "index"),array('escape'=>false)); ?>
              
            </li>
            <li class="tm nav-parent">
              <?php echo $this->Html->link("<i class='fa fa-bar-chart'></i> <span>Relatório 1</span>", array("controller"=>"answers","action" => "dashboard1"),array('escape'=>false)); ?>
              
            </li>
            <li class="tm nav-parent">
             <?php echo $this->Html->link("<i class='fa fa-pie-chart'></i> <span>Relatório 2</span>", array("controller"=>"answers","action" => "dashboard2"),array('escape'=>false)); ?>
            </li>
            <li class="tm nav-parent">
              <?php echo $this->Html->link("<i class='fa fa-power-off'></i> <span>Sair</span>", array("controller"=>"users","action" => "logout"),array('escape'=>false)); ?>
              
            </li>
          </ul>
          
          
        </div>
      </div>
