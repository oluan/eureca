        <!-- BEGIN TOPBAR -->
        <div class="topbar">
          <div class="header-left">       
            <div class="topnav">

             <a class="menutoggle" href="#" data-toggle="sidebar-collapsed"><span class="menu__handle"><span>Menu</span></span></a>

           </div>
         </div>
         <div class="header-right">
          <ul class="header-menu nav navbar-nav">
            
            <!-- BEGIN USER DROPDOWN -->
            <li class="dropdown" id="user-header">
              <a href="#" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                <?php echo $this->Html->image("../assets/images/avatars/semfoto.jpg", array('alt' => 'User'));?>
                <span class="username">Olá, Admin</span>
              </a>
              <ul class="dropdown-menu">
                <li>
                  <?php echo $this->Html->link("<i class='fa fa-power-off'></i> <span>Sair</span>", array("controller"=>"users","action" => "logout"),array('escape'=>false)); ?>
                </li>
              </ul>
            </li>
            <!-- END USER DROPDOWN -->
          </ul>
        </div>
        <!-- header-right -->
      </div>
        <!-- END TOPBAR -->