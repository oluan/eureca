<div class="header">
	<h2>Usuarios <strong>Gerenciar</strong></h2>
</div>
<div class="row">
	<div class="col-md-12 portlets">
		<div class="panel">
			<div class="panel-header panel-controls">
				<h3 class="pull-left"><i class="fa fa-pencil-square-o"></i> <strong>Usuários</strong> Editar</h3>
			</div>
			<div class="panel-content">

				<?php echo $this->Form->create('User', array('class'=>'form-horizontal')); ?>
				<?php echo $this->Form->input('id'); ?>
				<hr>
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-male"></i>			
								<?php echo $this->Form->input('firstName',array('class'=>'form-control form-white','placeholder'=>'Primeiro Nome','label'=>false)); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-gift"></i>			
								<?php echo $this->Form->input('lastName',array('class'=>'form-control form-white','placeholder'=>'Sobrenome','label'=>false)); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-envelope-o"></i>			
								<?php echo $this->Form->input('email',array('class'=>'form-control form-white','placeholder'=>'E-mail','label'=>false)); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-user"></i>			
								<?php echo $this->Form->input('username',array('class'=>'form-control form-white','placeholder'=>'Username para login','label'=>false)); ?>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12 prepend-icon">
								<i class="fa fa-key"></i>			
								<?php echo $this->Form->input('password',array('class'=>'form-control form-white','placeholder'=>'Senha','label'=>false)); ?>
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<?php echo $this->Form->input('type',array('class'=>'form-control form-white','placeholder'=>'Senha','label'=>false)); ?>
							</div>
						</div>
						
					</div>
				</div>
				<?php echo $this->Form->submit('Enviar',array('class'=>'btn btn-lg btn-success btn-block ladda-button',"data-style"=>"expand-left")) ?>
				<?php echo $this->Form->end(); ?>

			</div>
		</div>
	</div>
</div>
