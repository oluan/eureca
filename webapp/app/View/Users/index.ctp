<div class="header">
	<h2>Usuários <strong>Gerenciar</strong></h2>
</div>

<div class="row">
	<div class="col-md-12 portlets">
		<div class="panel">
			<div class="panel-header panel-controls">
				<h3 class="pull-left"><i class="fa fa-table"></i> <strong>Usuário</strong> Visão Geral</h3>
				
				<?php echo $this->Html->link(("<i class='fa fa-plus'></i> Usuário"), array('action' => 'add'),array("class"=>"btn btn-primary pull-right",'escape'=>false)); ?>
			</div>
			<div class="panel-content">

				<table class="table table-striped">
					<thead>
						<tr>
							<th><?php echo $this->Paginator->sort('id'); ?></th>
							<th><?php echo $this->Paginator->sort('email','E-mail'); ?></th>
							<th><?php echo $this->Paginator->sort('firstName','Primeiro Nome'); ?></th>
							<th><?php echo $this->Paginator->sort('lastName','Sobrenome'); ?></th>
							<th><?php echo $this->Paginator->sort('username'); ?></th>
							<th><?php echo $this->Paginator->sort('type_id','Tipo'); ?></th>
							<th class="actions"><?php echo __('Ações'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($users as $user): ?>
						<tr>
							<td><?php echo h($user['User']['id']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['email']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['firstName']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['lastName']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
							<td>
								<?php echo $user['Type']['title']; ?>
							</td>
							<td class="actions">
								
								<?php echo $this->Html->link(("<i class='fa fa-pencil-square-o'></i>"), array('action' => 'edit', $user['User']['id']),array("class"=>"btn btn-sm btn-warning",'escape'=>false)); ?>

								<?php echo $this->Form->postLink("<i class='fa fa-trash'></i>", 
									array('action' => 'delete', $user['User']['id']), 
									array("class"=>"btn btn-sm btn-danger",'escape'=>false), 
									array('confirm' => __('Tem certeza que deseja deletar usuário # %s?', $user['User']['id'] ))
									); 
									?>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			

				<p>
					<?php
					echo $this->Paginator->counter(array(
						'format' => __('Página {:page} de {:pages}, mostrando {:current} registros de {:count} , começando em {:start}, terminando em {:end}')
						));
						?>
					</p>
					<div class="paging">
						<ul class='pagination'>

							<?php
							echo $this->Paginator->prev('< ' . __('anterior'), array(), null, array('class' => 'prev disabled'));
							echo $this->Paginator->numbers(array('separator' => ''));
							echo $this->Paginator->next(__('próximo') . ' >', array(), null, array('class' => 'next disabled'));
							?>
						</ul>
					</div>

				</div>
			</div>
		</div>
	</div>

