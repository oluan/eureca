
<?php echo $this->Form->create('User', array('url' => 'login', 'class'=>'form-signin')) ?>

<div class="append-icon">
    <?php echo $this->Form->input('username', array('placeholder'=>'Username', 'label'=>false, 'class'=>'form-control form-white username','escape'=>false,"id"=>"username","required"=>true)) ?>
    <i class="icon-user"></i>
</div>
<div class="append-icon m-b-20">

    <?php echo $this->Form->input('password', array('placeholder'=>'Password', 'type' => 'password','label'=>false, 'class'=>'form-control form-white password','escape'=>false,"id"=>"password","required"=>true)) ?>
    <i class="icon-lock"></i>
</div>

<?php echo $this->Form->submit('Entrar',array('class'=>'btn btn-lg btn-danger btn-block ladda-button',"data-style"=>"expand-left")) ?>


<?php echo $this->Form->end(); ?>

