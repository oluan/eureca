<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		YESORNO
		<?php echo $this->fetch('title'); ?>
	</title>
	<link href="http://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet" type="text/css">

	<?php
	echo $this->Html->meta('icon');


	echo $this->Html->css('style.css');
	echo $this->Html->css('theme.css');
	echo $this->Html->css('ui.css');
	//echo $this->Html->css('layout.css');
	
	//echo $this->Html->css('../plugins/input-text/style.min.css');



	//JS
	echo $this->Html->script('../plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js');
	
	echo $this->Html->script('../plugins/jquery/jquery-1.11.1.min.js');
	echo $this->Html->script('../plugins/jquery/jquery-migrate-1.2.1.min.js');
	echo $this->Html->script('../plugins/gsap/main-gsap.min.js');
	echo $this->Html->script('../plugins/jquery-ui/jquery-ui-1.11.2.min.js');
	echo $this->Html->script('../plugins/jquery-block-ui/jquery.blockUI.min.js');
	echo $this->Html->script('../plugins/bootbox/bootbox.min.js');
	echo $this->Html->script('../plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js');
	echo $this->Html->script('../plugins/bootstrap/js/bootstrap.min.js');
	echo $this->Html->script('../plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js');
	echo $this->Html->script('../plugins/bootstrap-progressbar/bootstrap-progressbar.min.js');

	


	//echo $this->Html->css('dataTables.min.css');


	echo $this->fetch('meta');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
</head>
<body class="fixed-topbar sidebar-light color-blue theme-sdtd bg-light-blue">

	<?php echo $this->element('sidebar'); ?>
	<div class="main-content">
		<?php echo $this->element('topbar'); ?>

		<!-- BEGIN PAGE CONTENT -->
		<div class="page-content">
			
			<?php echo $this->Flash->render(); ?>

			<?php echo $this->fetch('content'); ?>

			<div class="footer">
				<div class="copyright">
					<p class="pull-left sm-pull-reset">
						<span>YESORNO APP. </span><span><a href="http://lsandrade.github.io/cv/">Luan Andrade</a></span>. <span>2016</span>
					</p>
					<p class="pull-right sm-pull-reset">
						<span><a href="https://www.linkedin.com/in/luan-andrade-5b59a653" class="m-r-10">Linkedin</a> | <a href="https://github.com/lsandrade" class="m-l-10 m-r-10">Github</a> | <a href="http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4311636H2" class="m-l-10">Lattes</a></span>
					</p>
				</div>
			</div>

		</div>
		<!-- END PAGE CONTENT -->
	</div><!-- end main content -->

	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
