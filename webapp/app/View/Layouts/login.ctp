<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        Bem Vindo ao YESORNO!! Entre agora!!
        <?php echo $this->fetch('title'); ?>
    </title>
    <link href="http://fonts.googleapis.com/css?family=Nothing+You+Could+Do" rel="stylesheet" type="text/css">

    <?php
    echo $this->Html->meta('icon');


    echo $this->Html->css('style.css');
    //echo $this->Html->css('theme.css');
    echo $this->Html->css('ui.css');

    //echo $this->Html->css('dataTables.min.css');


    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>

</head>
<body class="account separate-inputs" data-page="login">
    <!-- BEGIN LOGIN BOX -->
    <div class="container" id="login-block">
        <div class="row">
                <div class="text-center" style="padding-top:3%;color:white;"><h1>YESORNO</h1>
                    <h2>Descubra agora!!</h2></div>
            </div>
        <div class="row">
            

            
            <div class="col-sm-6 col-md-4 col-md-offset-4">
                <div class="account-wall">
                    <i class="user-img icons-faces-users-03"></i>
                    
                    <?php echo $this->Flash->render(); ?>

                    <?php echo $this->fetch('content'); ?>
                    
                </div>
            </div>
        </div>
        <p class="account-copyright">
            <span>YESORNO APP </span>- <span><a href="http://lsandrade.github.io/cv/">Luan Andrade</a></span> - <span>2016.</span>
        </p>

    </div>
    <!-- JS -->
    <?php

    echo $this->Html->script('../plugins/jquery/jquery-1.11.1.min.js');
    echo $this->Html->script('../plugins/jquery/jquery-migrate-1.2.1.min.js');
    echo $this->Html->script('../plugins/gsap/main-gsap.min.js');
    echo $this->Html->script('../plugins/bootstrap/js/bootstrap.min.js');
    echo $this->Html->script('../plugins/backstretch/backstretch.min.js');
    echo $this->Html->script('../plugins/bootstrap-loading/lada.min.js');
    echo $this->Html->script('../plugins/jquery-validation/jquery.validate.min.js');
    echo $this->Html->script('../plugins/jquery-validation/additional-methods.min.js');
    echo $this->Html->script('pages/login-v1.js');
    ?>
    
</body>
</html>