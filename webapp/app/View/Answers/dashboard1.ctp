<div class="header">
	<h2>Perguntas <strong>Relatório 1</strong></h2>
</div>

<div class="row">
	
	<?php echo $this->Form->create('Answer', array('class'=>'form-horizontal')); ?>

	<div class="col-md-10">
		<?php echo $this->Form->input('questions',array('type'=>'select','label'=>false,'class'=>'form-control form-white col-sm-9')); 
		?>
	</div>
	<div class="col-md-2">
		<input type="submit" class="btn btn-dark" value="enviar">
	</div>
	
</div>

<div class="row">
	<input type="hidden" value="<?php echo $question['Question']['id'];?>" id="id">

	<div class="col-md-6">
		<h3><i class="fa fa fa-bar-chart"></i> <strong>Relatório</strong> Coluna</h3>
		<p>Gráfico de colunas mostrando a contagem de respostas YES/NO
		</p>
		<div class="row">
			<div class="col-md-6">
				<div id="bar-chart" style="width:400px"></div>
			</div>
		</div>
	</div>


	<div class="col-md-6">
		<h3><i class="fa fa fa-pie-chart"></i> <strong>Relatório</strong> Pizza</h3>
		<p>Gráfico de pizza mostrando a contagem de respostas YES/NO
		</p>
		<div class="row">
			<div class="col-md-6">
				<div id="pie-chart" style="width:400px"></div>
			</div>
		</div>
	</div>


<?php echo $this->Html->script('../plugins/charts-highstock/js/highstock.min.js'); //financial charts

?>


<script>

var id = $('#id').val();

var url = "/eureca/answers/rest1/"+id;

$.ajax({
	method: 'POST',
	url: url,
	dataType: "json"
})
.done(function(results){
	console.log(results);

	$('#bar-chart').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: 'Respostas dos usuários'
		},
		xAxis: {
			categories: ['Opções']
		},
		yAxis: {
			title: {
				text: 'Respostas'
			}
		},
		series:results
	});


	$('#pie-chart').highcharts({
		chart: {
			type: 'pie',
			plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
		},
		title: {
			text: 'Respostas dos usuários'
		},

		 plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    //format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
		series:[{
			name: 'Respostas',
			colorByPoint: true,
			data:[
			{name:'YES', y:results[0].data[0]},
			{name:'NO', y:results[1].data[0]}
			]
		}]
	});
});

</script>
