<div class="header">
	<h2>Perguntas <strong>Relatório 2</strong></h2>
</div>


<div class="row">
	
	<div class="col-md-12">
		<h3><i class="fa fa fa-bar-chart"></i> <strong>Relatório</strong> YES</h3>
		<p>Gráfico de colunas mostrando a contagem de respostas por pergunta
		</p>
		<div class="row">
				<div id="yes-chart"></div>
			
		</div>
	</div>


	<div class="col-md-12">
		<h3><i class="fa fa fa-bar-chart"></i> <strong>Relatório</strong> NO</h3>
		<p>Gráfico de pizza mostrando a contagem de respostas por pergunta
		</p>
		<div class="row">
				<div id="no-chart"></div>
		</div>

	</div>


<?php echo $this->Html->script('../plugins/charts-highstock/js/highstock.min.js'); //financial charts

?>


<script>

var url = "/eureca/answers/rest2/";

$.ajax({
	method: 'POST',
	url: url,
	dataType: "json"
})
.done(function(results){
	console.log(results);

	$('#yes-chart').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: 'Respostas Positivas'
		},
		xAxis: {
			categories: ['Perguntas']
		},
		yAxis: {
			title: {
				text: 'Respostas'
			}
		},
		series:results.yes
	});


	$('#no-chart').highcharts({
		chart: {
			type: 'column'
		},
		title: {
			text: 'Respostas Negativas'
		},
		xAxis: {
			categories: ['Perguntas']
		},
		yAxis: {
			title: {
				text: 'Respostas'
			}
		},
		series:results.no
	});


});

</script>
