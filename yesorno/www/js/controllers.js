angular.module('your_app_name.controllers', [])

.controller('AuthCtrl', function($scope, $ionicConfig) {

})

// APP
.controller('AppCtrl', function($scope, $ionicConfig) {

})

//LOGIN
.controller('LoginCtrl', function($scope, $state, Backand,$http) {
	$scope.doLogin = function(users){
    console.log(users);

    var result = $http ({
      method: 'GET',
      url: Backand.getApiUrl() + '/1/query/data/login',
      params: {
        parameters: {
          username: users.username,
          password: users.password
        }
      }
    })
    .success(function(result){
      if(result[0].result>0)
       $state.go('app.tinder-cards',{user:result[0].id});
     else
      alert("Não há usuário com esse username/senha");
    console.log(result);
  })
    .error(function(result){
      alert('Ocorreu um erro. tente novamente');
    })
    ;
    

  };

})

.controller('SignupCtrl', function($scope, $state, Backand, $http) {
	
	$scope.doSignUp = function(users){

    console.log(users);

    $http({
      method: 'POST',
      url : Backand.getApiUrl() + '/1/objects/users',
      data: users,
      params: {
        returnObject: false
      }
    });

    $state.go('auth.login');
  };
})


// TINDER CARDS
.controller('TinderCardsCtrl', function($scope, $http,$stateParams, Backand) {

  var user = $stateParams.user;
  console.log(user);
  $scope.cards = [];


  $scope.addCard = function(question, description, id, user) {
    var newCard = {question: question, description: description, questionId: id, user: user};
    newCard.id = Math.random();
    $scope.cards.unshift(angular.extend({}, newCard));
  };

  $scope.addCards = function(user) {

    $http({
      method: 'GET',
      url: Backand.getApiUrl() + '/1/query/data/getQuestions',
      params: {
        parameters: {
          user: parseInt(user)
        }
      }
    })
    .success(function(value){
      console.log(value);
      angular.forEach(value, function (v) {
        $scope.addCard(v.title, v.description, v.id, user);
      });
    })
    ;

  };

  $scope.cardDestroyed = function(index) {
    $scope.cards.splice(index, 1);
    $scope.addCards(1);
  };

  $scope.transitionOut = function(card) {
    console.log('card transition out');
  };

  $scope.transitionRight = function(card) {

    $http({
      method: 'POST',
      url : Backand.getApiUrl() + '/1/objects/answers',
      data: {text:"yes",user_id:card.user,question_id:card.questionId},
      params: {
        returnObject: false
      }
    });

    console.log('card removed to the right');
    console.log(card);
  };

  $scope.transitionLeft = function(card) {

      $http({
      method: 'POST',
      url : Backand.getApiUrl() + '/1/objects/answers',
      data: {text:"no",user_id:card.user,question_id:card.questionId},
      params: {
        returnObject: false
      }
    });
    console.log('card removed to the left');
    console.log(card);
  };


  //$scope.addFirstCards();
  $scope.addCards(user);

})




;
